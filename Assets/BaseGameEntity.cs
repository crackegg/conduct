﻿using Assets.States;
using UnityEngine;

namespace Assets
{
    public class BaseGameEntity<T> : MonoBehaviour
    {
        public StateMachine<T> StateMachine { get; set; }

        public long Id;

        public Rigidbody2D Rigidbody;
        public AudioSource AudioSource;
        public Animator Animator;
        public Collider2D Collider;
        public Transform GroundCheck;
    }
}
