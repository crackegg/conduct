﻿using UnityEngine;
using UnityStandardAssets._2D;
using Assets.ScriptableObjects;
using Assets.States;
using Assets.States.EnergyManagers;

public class EnergyManager : MonoBehaviour
{
    public StateMachine<EnergyManager> StateMachine;

    public int InitialEnergy = 0;
    public int CurrentEnergy = 0;
    public int movementConsumption = 1;
    public int JumpConsumption = 100;
    public int foudrePower = 200;
    public string JumpInput = "Jump_P1";
    public string HorizontalInput = "Horizontal_P1";
    public Platformer2DUserControl Control;
    public PlatformerCharacter2D Character;
    public Animator Animator;
    public AudioSource AudioSource;
    public SpriteRenderer Gauge;
    private float delay = 0.5f;
    private float lastCharged = -10;
    public AudioClips AudioClips;

    // Use this for initialization

    void Start()
    {
        CurrentEnergy = InitialEnergy;
        Control = GetComponent<Platformer2DUserControl>();
        Animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();

        StateMachine = new StateMachine<EnergyManager>(this);

        StateMachine.CurrentState = EmptyEnergyTankState.Instance();
        StateMachine.GlobalState = RemoveEnergyOnActionsState.Instance();
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine.Update();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Foudre") && Time.time > lastCharged + delay)
        {
            CurrentEnergy = Mathf.Min(foudrePower, CurrentEnergy + foudrePower);
            lastCharged = Time.time;
        }
    }
    public void RemoveEnergy(int amount)
    {
        CurrentEnergy = Mathf.Max(0, CurrentEnergy - amount);
    }

    public void CheckIfPowered()
    {
        Control.enabled = CurrentEnergy > 0;
        Animator.SetInteger("currentEnergy", CurrentEnergy);
    }

    public void PlayPowerUpClip()
    {
        AudioSource.clip = AudioClips.RobotPowerUpClip;
        AudioSource.loop = false;
        AudioSource.Play();
    }

    public void PlayPowerDownClip()
    {
        AudioSource.clip = AudioClips.RobotPowerDownClip;
        AudioSource.loop = false;
        AudioSource.Play();
    }
}
