﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
    public const int power = 10;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Nuage"))
        {
            other.GetComponent<ChargeManager>().AddCharge(power);
            Destroy(gameObject);            
        }
    }
}
