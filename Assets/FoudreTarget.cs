﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoudreTarget : MonoBehaviour {
    public delegate void OnFoudreHit();
    public event OnFoudreHit OnHit;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Foudre"))
        {
            OnHit();
        }
    }
}
