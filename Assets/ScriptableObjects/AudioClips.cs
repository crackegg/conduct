﻿using UnityEngine;

namespace Assets.ScriptableObjects
{
    public class AudioClips : ScriptableObject
    {
        public AudioClip PushClip;
        public AudioClip FootStepsClip;
        public AudioClip JumpClip;
        public AudioClip LandClip;

        public AudioClip GuideForLoopFanClip;
        public AudioClip HeaderFanClip;
        public AudioClip LoopFanClip;
        public AudioClip StopsFanCLip;

        public AudioClip LevelCompleteClip;
        public AudioClip LevelIntroGuideClip;
        public AudioClip LevelIntroHeaderClip;
        public AudioClip LevelLoopClip;

        public AudioClip SwitchActivationClip;

        public AudioClip RobotPowerUpClip;
        public AudioClip RobotPowerDownClip;

        public AudioClip LightningBoltClip;

        public AudioClip ButtonPowerUp;
        public AudioClip ButtonPowerDown;
    }
}