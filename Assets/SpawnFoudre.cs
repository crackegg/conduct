﻿using Assets.ScriptableObjects;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
public class SpawnFoudre : MonoBehaviour
{
    public GameObject dFoudre;
    public AudioSource AudioSource;
    private ChargeManager charge;
    private ParticleSystem spark;
    public const float COOLDOWN = 1.5f;
    private float attackTime;
    private float foudreHeight;
    public string Thunderstrike = "Thunderstrike";

    public AudioClips AudioClips;
    // Use this for initialization
    void Start()
    {
        attackTime = Time.time - COOLDOWN;
        charge = GetComponent<ChargeManager>();
        spark = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown(Thunderstrike) && Time.time > attackTime + COOLDOWN && charge.Attack())
        {
            ActivateFoudre();
        }
    }

    private void ActivateFoudre()
    {
        attackTime = Time.time;
        spark.Play();
        PlayLightningBoltSound();
        GameObject initial = Instantiate(dFoudre);
        initial.transform.parent = gameObject.transform;
        initial.transform.localPosition = new Vector3(0, 0);
    }

    private void PlayLightningBoltSound()
    {
        AudioSource.clip = AudioClips.LightningBoltClip;
        AudioSource.loop = false;
        AudioSource.Play();
    }
}
