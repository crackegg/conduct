﻿using UnityEngine;

public class ChargeManager : MonoBehaviour {
    public int Charge = 1000;
    public const int energyRequired = 100;
    public void AddCharge(int amount)
    {
        Charge += amount;
        Debug.Log("power left to cloud: " + Charge);
    }
    public bool Attack()
    {
        if(Charge >= energyRequired)
        {
            Charge -= energyRequired;
            Debug.Log("power left to cloud: " + Charge);
            return true;
        }
        return false;
    }
}
