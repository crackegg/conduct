﻿using UnityEngine;

namespace Assets.States
{
    public class GroundedState : State<Robot>
    {
        private static GroundedState _instance;

        public static GroundedState Instance()
        {
            if (_instance == null)
                _instance = new GroundedState();

            return _instance;
        }

        public override void Enter(Robot entity)
        {
        }

        public override void Execute(Robot entity)
        {
            if (Input.GetButtonDown("Jump_P1"))
                entity.StateMachine.ChangeState(JumpingState.Instance());
        }

        public override void FixedExecute(Robot entity)
        {
        }

        public override void Exit(Robot entity)
        {
        }
    }
}
