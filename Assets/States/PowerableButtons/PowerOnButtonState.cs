﻿using UnityEngine;

namespace Assets.States.PowerableButtons
{
    public class PowerOnButtonState : State<PowerableButton>
    {
        private static PowerOnButtonState _instance;

        public static PowerOnButtonState Instance()
        {
            if (_instance == null)
                _instance = new PowerOnButtonState();

            return _instance;
        }

        public override void Enter(PowerableButton entity)
        {
            entity.Animator.SetBool("IsPowered", true);
            entity.AudioSource.clip = entity.AudioClips.ButtonPowerUp;
            entity.AudioSource.loop = false;

            entity.AudioSource.Play();
        }

        public override void Execute(PowerableButton entity)
        {
            if (entity.Activable.IsReady() && entity.NextToAnActivable && Input.GetButtonDown("Activate"))
            {
                entity.Activable.Activate();
                entity.Animator.SetTrigger("Pushed");
            }
        }

        public override void Exit(PowerableButton entity)
        {
        }

        public override void FixedExecute(PowerableButton entity)
        {
        }
    }
}
