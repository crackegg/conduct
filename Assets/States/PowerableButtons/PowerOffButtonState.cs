﻿using Assets.ScriptableObjects;
using UnityEngine;

namespace Assets.States.PowerableButtons
{
    public class PowerOffButtonState : State<PowerableButton>
    {
        private static PowerOffButtonState _instance;

        public static PowerOffButtonState Instance()
        {
            if (_instance == null)
                _instance = new PowerOffButtonState();

            return _instance;
        }
        
        public override void Enter(PowerableButton entity)
        {
            entity.Activable.Desactivate();
            entity.Animator.SetBool("IsPowered", false);
            entity.AudioSource.clip = entity.AudioClips.ButtonPowerDown;
            entity.AudioSource.loop = false;

            entity.AudioSource.Play();
        }

        public override void Execute(PowerableButton entity)
        {
            if (entity.Activable.IsReady() && entity.NextToAnActivable && Input.GetButtonDown("Activate"))
            {
                entity.Animator.SetTrigger("Pushed");
            }
        }

        public override void Exit(PowerableButton entity)
        {
        }

        public override void FixedExecute(PowerableButton entity)
        {
        }
    }
}
