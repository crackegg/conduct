﻿using Assets.ScriptableObjects;

namespace Assets.States
{
    public class PushingState : State<Robot>
    {
        private static PushingState _instance;

        public static PushingState Instance()
        {
            if (_instance == null)
                _instance = new PushingState();

            return _instance;
        }

        public override void Enter(Robot entity)
        {
            entity.AudioSource.clip = entity.AudioClips.PushClip;
            entity.AudioSource.loop = true;
            entity.AudioSource.Play();
        }

        public override void Execute(Robot entity)
        {
            if (entity.Rigidbody.velocity.x == 0)
                entity.StateMachine.ChangeState(GroundedState.Instance());
        }

        public override void FixedExecute(Robot entity)
        {
        }

        public override void Exit(Robot entity)
        {
            entity.AudioSource.Stop();
        }
    }
}
