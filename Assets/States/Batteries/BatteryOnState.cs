﻿using System;

namespace Assets.States.Batteries
{
    public class BatteryOnState : State<Battery>
    {
        private static BatteryOnState _instance;

        public static BatteryOnState Instance()
        {
            if (_instance == null)
                _instance = new BatteryOnState();

            return _instance;
        }

        public override void Enter(Battery entity)
        {
            entity.CurrentEnergyLevel = entity.MaxEnergyLevel;
            entity.Powerable.PowerOn();
            entity.IsHittedByLightning = false;
        }

        public override void Execute(Battery entity)
        {
        }

        public override void Exit(Battery entity)
        {
        }

        public override void FixedExecute(Battery entity)
        {
            if (entity.CurrentEnergyLevel <= 0)
                entity.StateMachine.ChangeState(BatteryOffState.Instance());

            entity.CurrentEnergyLevel -= 0.01f;
        }
    }
}
