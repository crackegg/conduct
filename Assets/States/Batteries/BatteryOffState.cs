﻿using System;

namespace Assets.States.Batteries
{
    public class BatteryOffState : State<Battery>
    {
        private static BatteryOffState _instance;

        public static BatteryOffState Instance()
        {
            if (_instance == null)
                _instance = new BatteryOffState();

            return _instance;
        }

        public override void Enter(Battery entity)
        {
            entity.Animator.SetBool("PowerOff", true);
            entity.Powerable.PowerOff();
            entity.IsHittedByLightning = false;
        }

        public override void Execute(Battery entity)
        {
        }

        public override void Exit(Battery entity)
        {
            entity.Animator.SetTrigger("Hit");
        }

        public override void FixedExecute(Battery entity)
        {
            if (entity.IsHittedByLightning)
            {
                entity.StateMachine.ChangeState(BatteryOnState.Instance());
            }
        }
    }
}
