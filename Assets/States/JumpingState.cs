﻿using System;
using Assets.ScriptableObjects;
using UnityEngine;

namespace Assets.States
{
    public class JumpingState : State<Robot>
    {
        private static JumpingState _instance;

        public static JumpingState Instance()
        {
            if (_instance == null)
                _instance = new JumpingState();

            return _instance;
        }

        public override void Enter(Robot entity)
        {
            entity.AudioSource.loop = false;
            entity.AudioSource.clip = entity.AudioClips.JumpClip;
            entity.AudioSource.Play();
        }

        public override void Execute(Robot entity)
        {
            if (CheckIfGrounded(entity))
                entity.StateMachine.ChangeState(GroundedState.Instance());
        }

        public override void Exit(Robot entity)
        {
            entity.AudioSource.loop = false;
            entity.AudioSource.clip = entity.AudioClips.LandClip;
            entity.AudioSource.Play();
        }

        public bool CheckIfGrounded(Robot entity)
        {
            return Physics2D.Linecast(entity.transform.position, entity.GroundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        }

        public override void FixedExecute(Robot entity)
        {
            throw new NotImplementedException();
        }
    }
}
