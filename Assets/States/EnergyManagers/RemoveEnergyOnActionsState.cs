﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Assets.States.EnergyManagers
{
    public class RemoveEnergyOnActionsState : State<EnergyManager>
    {
        private static RemoveEnergyOnActionsState _instance;

        public static RemoveEnergyOnActionsState Instance()
        {
            if (_instance == null)
                _instance = new RemoveEnergyOnActionsState();

            return _instance;
        }

        public override void Enter(EnergyManager entity)
        {
        }

        public override void Execute(EnergyManager entity)
        {
            if (Input.GetAxis(entity.HorizontalInput) != 0)
            {
                entity.RemoveEnergy(entity.movementConsumption);
            }
            if (CrossPlatformInputManager.GetButtonDown(entity.JumpInput))
            {
                entity.RemoveEnergy(entity.JumpConsumption);
            }

            var ratio = ((float)entity.CurrentEnergy / (float)entity.foudrePower);
            entity.Gauge.color = Color.Lerp(Color.red, Color.green, ratio);
        }

        public override void Exit(EnergyManager entity)
        {
        }

        public override void FixedExecute(EnergyManager entity)
        {
        }
    }
}
