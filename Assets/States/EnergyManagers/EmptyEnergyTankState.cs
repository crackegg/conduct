﻿using System;

namespace Assets.States.EnergyManagers
{
    public class EmptyEnergyTankState : State<EnergyManager>
    {
        private static EmptyEnergyTankState _instance;

        public static EmptyEnergyTankState Instance()
        {
            if (_instance == null)
                _instance = new EmptyEnergyTankState();

            return _instance;
        }

        public override void Enter(EnergyManager entity)
        {
            entity.Control.StopMovement();
            entity.PlayPowerDownClip();
        }

        public override void Execute(EnergyManager entity)
        {
            if (entity.CurrentEnergy > 0)
                entity.StateMachine.ChangeState(TankWithEnergyState.Instance());

            entity.CheckIfPowered();
        }

        public override void Exit(EnergyManager entity)
        {
        }

        public override void FixedExecute(EnergyManager entity)
        {
        }
    }
}
