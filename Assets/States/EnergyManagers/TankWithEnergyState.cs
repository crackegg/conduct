﻿namespace Assets.States.EnergyManagers
{
    public class TankWithEnergyState : State<EnergyManager>
    {
        private static TankWithEnergyState _instance;

        public static TankWithEnergyState Instance()
        {
            if (_instance == null)
                _instance = new TankWithEnergyState();

            return _instance;
        }

        public override void Enter(EnergyManager entity)
        {
            entity.PlayPowerUpClip();
        }

        public override void Execute(EnergyManager entity)
        {
            if (entity.CurrentEnergy <= 0)
                entity.StateMachine.ChangeState(EmptyEnergyTankState.Instance());

            entity.CheckIfPowered();
        }

        public override void Exit(EnergyManager entity)
        {
        }

        public override void FixedExecute(EnergyManager entity)
        {
        }
    }
}
