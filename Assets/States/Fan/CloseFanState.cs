﻿namespace Assets.States.Fan
{
    public class CloseFanState : State<Fan>
    {
        private static CloseFanState _instance;

        public static CloseFanState Instance()
        {
            if (_instance == null)
                _instance = new CloseFanState();

            return _instance;
        }

        public override void Enter(Fan entity)
        {
            entity.Animator.SetBool("IsActive", false);
            entity.Collider.enabled = false;
            entity.WindParticleSystem.Stop();
            entity.SubWindParticleSystem.Stop();
        }

        public override void Execute(Fan entity)
        {
        }

        public override void FixedExecute(Fan entity)
        {
        }

        public override void Exit(Fan entity)
        {
            entity.Collider.enabled = true;
        }
    }
}
