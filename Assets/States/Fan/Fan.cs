﻿using Assets.ScriptableObjects;
using System;
using UnityEngine;

namespace Assets.States.Fan
{
    public class Fan : BaseGameEntity<Fan>, IActivable
    {
        public FanAudioManager FanAudioManager;
        public ParticleSystem WindParticleSystem;
        public ParticleSystem SubWindParticleSystem;

        public AudioClips AudioClips;

        public void Start()
        {
            WindParticleSystem.Stop();
            SubWindParticleSystem.Stop();

            StateMachine = new StateMachine<Fan>(this);

            StateMachine.CurrentState = CloseFanState.Instance();
        }
        
        public void Activate()
        {

            if (StateMachine.IsInState(ActiveFanState.Instance()))
                Desactivate();
            else
                StateMachine.ChangeState(ActiveFanState.Instance());
        }

        public bool IsReady()
        {
            return true;
        }

        public void Desactivate()
        {
            StateMachine.ChangeState(CloseFanState.Instance());
        }
    }
}
