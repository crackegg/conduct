﻿using Assets.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.States.Fan
{
    public class FanAudioManager : MonoBehaviour
    {
        public List<long> EntityIds;
        public AudioSource AudioSource;

        public AudioClips AudioClips;

        public void AddFanEntity(long id)
        {
            if (EntityIds.Count == 0)
                Invoke("PlayLoop", AudioClips.GuideForLoopFanClip.length);

            EntityIds.Add(id);
        }

        public void RemoveEntity(long id)
        {
            EntityIds.Remove(id);

            if (EntityIds.Count == 0)
                AudioSource.Stop();            
        }

        private void PlayLoop()
        {
            AudioSource.loop = true;
            AudioSource.clip = AudioClips.LoopFanClip;
            AudioSource.Play();
        }
    }
}
