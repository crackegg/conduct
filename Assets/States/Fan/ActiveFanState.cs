﻿using Assets.ScriptableObjects;

namespace Assets.States.Fan
{
    public class ActiveFanState : State<Fan>
    {
        private static ActiveFanState _instance;
        
        public static ActiveFanState Instance()
        {
            if (_instance == null)
                _instance = new ActiveFanState();

            return _instance;
        }

        public override void Enter(Fan entity)
        {
            entity.Animator.SetBool("IsActive", true);
            entity.AudioSource.clip = entity.AudioClips.HeaderFanClip;
            entity.AudioSource.Play();

            entity.FanAudioManager.AddFanEntity(entity.Id);
            entity.WindParticleSystem.Play();
            entity.SubWindParticleSystem.Play();
        }

        public override void Execute(Fan entity)
        {
        }

        public override void FixedExecute(Fan entity)
        {
        }

        public override void Exit(Fan entity)
        {
            entity.AudioSource.clip = entity.AudioClips.StopsFanCLip;
            entity.AudioSource.Play();
            entity.FanAudioManager.RemoveEntity(entity.Id);
        }
    }
}
