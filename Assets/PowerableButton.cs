﻿using Assets.ScriptableObjects;
using Assets.States;
using Assets.States.PowerableButtons;
using UnityEngine;

namespace Assets
{
    public class PowerableButton : MonoBehaviour , IPowerable
    {
        private StateMachine<PowerableButton> _stateMachine;

        public IActivable Activable;  

        public bool NextToAnActivable;
        public GameObject ActivableGameObject;
        public AudioSource AudioSource;
        public Animator Animator;

        public AudioClips AudioClips;

        public void Start()
        {
            Activable = ActivableGameObject.GetComponent<IActivable>();
            Animator = GetComponent<Animator>();

            _stateMachine = new StateMachine<PowerableButton>(this);
            _stateMachine.CurrentState = PowerOffButtonState.Instance();
        }

        public void Update()
        {
            _stateMachine.Update();
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                NextToAnActivable = true;
            }
        }

        public void OnTriggerExit2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                NextToAnActivable = false;
            }
        }

        public void PowerOff()
        {
            _stateMachine.ChangeState(PowerOffButtonState.Instance());
        }

        public void PowerOn()
        {
            _stateMachine.ChangeState(PowerOnButtonState.Instance());
        }
    }

    public interface IActivable
    {
        void Activate();
        void Desactivate();
        bool IsReady();
    }
}
