﻿using Assets.ScriptableObjects;
using UnityEngine;

namespace Assets
{
    public class Robot : BaseGameEntity<Robot>
    {
        private bool LandingSoundWasPlayed = false;

        private Rigidbody2D _pushableRigidbody;
        private Rigidbody2D _robotRigidbody;

        public AudioSource RunAudioSource;
        public AudioSource JumpAndLandAudioSource;
        public AudioSource PushAudioSource;

        public AudioClips AudioClips;

        public void Start()
        {
            _robotRigidbody = GetComponent<Rigidbody2D>();
        }

        public void Update()
        {
            
        }

        public void PlayJump()
        {

            LandingSoundWasPlayed = false;
            JumpAndLandAudioSource.Stop();
            JumpAndLandAudioSource.loop = false;
            JumpAndLandAudioSource.clip = AudioClips.JumpClip;
            JumpAndLandAudioSource.Play();
        }

        public void PlayLand()
        {
            if (LandingSoundWasPlayed) return;

            LandingSoundWasPlayed = true; 
            JumpAndLandAudioSource.loop = false;
            JumpAndLandAudioSource.clip = AudioClips.LandClip;
            JumpAndLandAudioSource.Play();
        }

        public void PlayWalk()
        {
            RunAudioSource.Stop();
            RunAudioSource.loop = false;
            RunAudioSource.clip = AudioClips.FootStepsClip;
            RunAudioSource.Play();
        }

        public void PlayPush()
        {
            PushAudioSource.loop = true;
            PushAudioSource.clip = AudioClips.PushClip;
            PushAudioSource.Play();
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Pushable")) {
                _pushableRigidbody = collider.gameObject.GetComponent<Rigidbody2D>();
            }
        }

        public void OnTriggerExit2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Pushable")) {
                _pushableRigidbody = null;
            }
        }
    }
}
