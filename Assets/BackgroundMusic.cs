﻿using Assets.ScriptableObjects;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour {

    public AudioSource EventAudioSource;
    public AudioSource LoopAudioSource;

    public AudioClips AudioClips;

    public void Start()
    {
        Invoke("PlayLoop", AudioClips.LevelIntroGuideClip.length);
        PlayHeader();
    }

    public void PlayHeader()
    {
        EventAudioSource.loop = false;
        EventAudioSource.clip = AudioClips.LevelIntroHeaderClip;
        EventAudioSource.Play();
    }

    public void PlayLoop()
    {
        LoopAudioSource.loop = true;
        LoopAudioSource.clip = AudioClips.LevelLoopClip;
        LoopAudioSource.Play();
    }

}
