﻿using Assets.States.Batteries;
using UnityEngine;

namespace Assets
{
    public class Battery : BaseGameEntity<Battery>
    {
        public float MaxEnergyLevel;
        public bool IsFullCharge;
        public GameObject PowerableGameObject;

        public float CurrentEnergyLevel;
        public IPowerable Powerable;

        public bool IsHittedByLightning;

        public void Start()
        {
            StateMachine = new States.StateMachine<Battery>(this);

            StateMachine.CurrentState = BatteryOffState.Instance();

            Powerable = PowerableGameObject.GetComponent<IPowerable>();
        }

        public void Update()
        {
            StateMachine.Update();
        }

        public void FixedUpdate()
        {
            StateMachine.FixedUpdate();
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Lightning"))
            {
                IsHittedByLightning = true;
            }
        }
    }

    public interface IPowerable
    {
        void PowerOn();
        void PowerOff();
    }
}
