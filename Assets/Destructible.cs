﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {
    public bool robotInRange = false;
    public GameObject toSpawn;
    public GameObject repulsor;
    public int amountToSpawn = 1;
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            robotInRange = true;
        }
    }
    public void OnTriggerExit2D(Collider2D other){
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            robotInRange = false;
        }
    }
	// Update is called once per frame
	void Update () {
		if(robotInRange && Input.GetButtonDown("Activate"))
        {
            Interact();
        }
	}
    private void Interact()
    {
        for (int i = 0; i < amountToSpawn; i++)
        {
            GameObject proton = Instantiate(toSpawn);
            proton.transform.position = gameObject.transform.position;
            proton.transform.Translate(new Vector3(Random.Range(0, 100)/100.00f, Random.Range(0, 100)/100.00f));
        }
        GameObject boom = Instantiate(repulsor);
        boom.transform.position = gameObject.transform.position;
        Destroy(gameObject);
    }
}
