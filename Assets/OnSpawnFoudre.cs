﻿using UnityEngine;

public class OnSpawnFoudre : MonoBehaviour {

    public const float FOUDRE_DURATION = 0.5f;
    private float spawnTime;
    private BoxCollider2D _collider;
    private bool isColliding = false;

	void Start () {
        spawnTime = Time.time;
        _collider = GetComponent<BoxCollider2D>();
        gameObject.transform.localScale = new Vector3(1, 1, 1);

        if (!getColliding())
        {
            GameObject nextFoudre = Instantiate(gameObject);
            nextFoudre.transform.parent = gameObject.transform;
            nextFoudre.transform.localPosition = new Vector3(0, -getSpriteHeight());
            nextFoudre.SetActive(true);
        }
    }

    void Update()
    {
        if(Time.time > spawnTime + FOUDRE_DURATION)
        {
            Destroy(gameObject);
        }
    }

    public bool getColliding()
    {
        var others =  Physics2D.OverlapBoxAll(gameObject.transform.position, _collider.size, 0);
        for (int i = 0; i < others.Length; i++)
        {
            if (others[i].CompareTag("Block"))
            {
                return true;
            }
        }
        return false;
    }
    public float getSpriteHeight()
    {
        return _collider.size.y;
    }
}
