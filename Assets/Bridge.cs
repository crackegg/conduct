﻿using System;
using Assets;
using UnityEngine;

public class Bridge : MonoBehaviour, IActivable {

    public HingeJoint2D HingeJoint;
    public AudioSource AudioSource;

    public AudioClip PullUpClip;
    public AudioClip PullDownClip;

    private bool _isReady;
    private bool _isActive;

    public void Start()
    {
        _isReady = true;
    }

    public void Activate()
    {
        _isReady = false;
        if (_isActive)
        {
            Desactivate();
        }else
        {
            PullDown();
            PlayClip(PullDownClip);
            _isActive = true;
        }

        HingeJoint.useMotor = true;
    }

    private void PullUp()
    {
        var motor = HingeJoint.motor;

        motor.motorSpeed = -65;

        HingeJoint.motor = motor;
    }

    private void PullDown()
    {
        var motor = HingeJoint.motor;

        motor.motorSpeed = 133;

        HingeJoint.motor = motor;
    }

    private void PlayClip(AudioClip clip)
    {
        AudioSource.loop = false;
        AudioSource.clip = clip;
        Invoke("ReadyUp", clip.length);

        AudioSource.Play();
    }

    private void ReadyUp()
    {
        _isReady = true;
    }

    public bool IsReady()
    {
        return _isReady;
    }

    public void Desactivate()
    {
        if (_isActive)
        {
            PullUp();
            PlayClip(PullUpClip);
            _isActive = false;
        }
    }
}
