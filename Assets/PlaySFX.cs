﻿using UnityEngine;

namespace Assets
{
    public class PlaySFX : MonoBehaviour
    {
        public AudioClip Clip;
        public AudioSource AudioSource;

        public void Play()
        {
            AudioSource.loop = false;
            AudioSource.clip = Clip;

            AudioSource.Play();
        }
    }
}
